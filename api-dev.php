<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 07.10.2016
 * Time: 19:01
 */

global $wpdb;
error_reporting(E_ALL);
ini_set('display_errors', 1);


class twitchCRED {
    private $code = "";
    public $twitchid = ""; // DGI_Bot Client ID
    public $response = array();
    private $mycred;

    function __construct($givenKey)
    {
        if($givenKey != $this->code) {
            $this->response["error"] = "Bad API key";
            $this->response["error-code"] = 101;
            $this->push_response();
        }

        $this->mycred = mycred();
    }

    public function getWPID($twitchName) {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM wp_users WHERE user_nicename='$twitchName'");
        if(count($res) == 0)
            return false;
        else {
            return $res[0]->ID;
        }
    }



    public function update_creds($twitchName, $creds, $desc) {
        $f = $this->getWPID($twitchName);
        if($f != false) {
            mycred_add( "twitchCRED", $f, $creds, $desc );
        }
        return $f;
    }

    public function get_creds($twitchName) {
        $f = $this->getWPID($twitchName);
        if($f == false) {
            $this->response["error"] = "Couldn't find a member " . $twitchName;
            $this->response["error-code"] = 105;
        }
        $c = $this->mycred->get_users_balance($f);
        $this->response["credits"] = $c;
        $this->response["response-time"] = time();

        $this->push_response();
    }

    public function push_response() {
        die(json_encode($this->response));
    }
}

require_once("../../../wp-load.php");
if(!isset($_GET["code"]))
    new twitchCRED("");

$cred = new twitchCRED($_GET["code"]);

if(!isset($_GET["action"])) {
    $cred->response["error"] = "Bad action";
    $cred->response["error-code"] = 102;
    $cred->push_response();
}

function dberr($err) {
    global $cred;
    $cred->response["error"] = $err;
    $cred->response["error-code"] = 1;
    $cred->push_response();
}

switch($_GET["action"]) {
    // Modules
    case "get_whisper_messages":
        $messages = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_whispers", OBJECT);
        $cred->response["messages"] = $messages;
        $cred->response["done"] = true;
        $cred->response["response-time"] = time();
        $cred->push_response();
        break;

    // General
    case "check":
        if(!isset($_GET["streamer"])) {
            $cred->response["error"] = "Missing streamer property";
            $cred->response["error-code"] = 103;
            $cred->push_response();
        }
        else if(!isset($_GET["viewer"])) {
            $cred->response["error"] = "Missing viewer property";
            $cred->response["error-code"] = 104;
            $cred->push_response();
        }

        $streamer = $_GET["streamer"];
        $viewer = $_GET["viewer"];


        // Check if already exists
        $res = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_online WHERE watcher='$viewer' AND streamer='$streamer'");
        if(count($res) == 1) {
            // Already exists, so just update
            $c = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='increase_points'", OBJECT);
            $points = $c[0]->cfg_value;


            if($viewer != $streamer) {
                if($cred->update_creds($viewer, $points, "For watching " . $streamer) || tcred_debug()) {
                    $r = $wpdb->query("UPDATE wp_sqyy0tvj8s_tcred_online SET coins_earned='" . ($res[0]->coins_earned + $points)  . "', updated='" . time() . "'
                    WHERE ID='" . $res[0]->ID . "'") or dberr($wpdb->last_error);
                    $cred->response["points_increased"] = $points;
                    $cred->response["points_session"] = ($res[0]->coins_earned + $points);
                }
                else {
                    $cred->response["warning"] = "Didn't added any coins - the viewer is not registered on the DGI website";
                    $cred->response["warning-code"] = 1;
                }
            }
            else {
                $cred->response["warning"] = "Didn't added any coins - can't add points for watching yourself";
                $cred->response["warning-code"] = 2;
            }

            $cred->response["done"] = true;
            $cred->response["check_action"] = "update";

            $cred->push_response();
        }
        else {
            // Insert new
            $r = $wpdb->query( "INSERT INTO wp_sqyy0tvj8s_tcred_online (streamer, watcher, updated, start_time)
            VALUES ('$streamer', '$viewer', '". time() . "', '" . time() . "')" ) or dberr($wpdb->last_error);

            $cred->response["done"] = true;
            $cred->response["response-time"] = time();
            $cred->response["check_action"] = "new";
            $cred->push_response();
        }

        break;
    case "left":
        if(!isset($_GET["streamer"])) {
            $cred->response["error"] = "Missing streamer property";
            $cred->response["error-code"] = 103;
            $cred->push_response();
        }
        else if(!isset($_GET["viewer"])) {
            $cred->response["error"] = "Missing viewer property";
            $cred->response["error-code"] = 104;
            $cred->push_response();
        }

        $streamer = $_GET["streamer"];
        $viewer = $_GET["viewer"];
        $res = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_online WHERE watcher='$viewer' AND streamer='$streamer'");

        $r = $wpdb->query("DELETE FROM wp_sqyy0tvj8s_tcred_online WHERE watcher='$viewer' AND streamer='$streamer'")
        or dberr($wpdb->last_error);
        $cred->response["done"] = true;
        $cred->response["response-time"] = time();
        $cred->response["earned_points"] = $res[0]->coins_earned;

        $datediff = date_diff(date_create(date("Y-m-d G:i:s", $res[0]->start_time)), date_create(date("Y-m-d G:i:s")));

        $months = $datediff->m + ($datediff->y > 1 ? ($datediff->y * 12) : 0);
        $days   = $datediff->d;

        $format = "";

        if($days > 0) {
            if ($datediff->y > 0) $format .= $datediff->y . ($datediff->y == 1 ? " Year, " : " Years, ");
            if ($months > 0) $format .= $months . ($months == 1 ? " Month, " : " Months, ");
            $format .= $days . ($days == 1 ? " Day" : " Days");
        }
        else {
            if($datediff->h > 0) $format .= $datediff->h . ($datediff->h == 1 ? " Hour, " : " Hours, ");
            if($datediff->i > 0) $format .= $datediff->i . ($datediff->i == 1 ? " Minute, " : " Minutes, ");
            if($datediff->s > 0) $format .= $datediff->s . ($datediff->s == 1 ? " Second" : " Seconds");
        }
        $cred->response["session_time"] = $format;
        $cred->push_response();

        break;
    case "settings":
        $c = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='increase_time'", OBJECT);
        $time = $c[0]->cfg_value;
        $c = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='increase_points'", OBJECT);
        $points = $c[0]->cfg_value;
        $c = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='check_time'", OBJECT);
        $checkTime = $c[0]->cfg_value;

        $cred->response["increase_time"] = $time;
        $cred->response["increase_points"] = $points;
        $cred->response["check_time"] = $checkTime;

        $cred->response["response-time"] = time();
        $cred->push_response();
        break;

    case "get":
        if(!isset($_GET["viewer"])) {
            $cred->response["error"] = "Missing viewer property";
            $cred->response["error-code"] = 104;
            $cred->push_response();
        }

        $viewer = $_GET["viewer"];
        $cred->get_creds($viewer);
        break;
    case "reset":
        $wpdb->query("TRUNCATE TABLE wp_sqyy0tvj8s_tcred_streams") // EMPTY VIEWERS
            or dberr($wpdb->last_error);
        $wpdb->query("TRUNCATE TABLE wp_sqyy0tvj8s_tcred_online") // EMPTY STREAMS
            or dberr($wpdb->last_error);

        $cred->response["done"] = true;
        $cred->response["response-time"] = time();
        $cred->push_response();
        break;
    case "streamers_check":
        if(!isset($_GET["watching"])) $watching = "";
        else $watching = $_GET["watching"];

        $users = $wpdb->get_results("SELECT * FROM wp_users");
        $streams = "";
        foreach($users as $user) {
            $streams .= $user->user_nicename . ",";
        }
        $tapi = json_decode(file_get_contents("https://api.twitch.tv/kraken/streams?channel=$streams&client_id=" . $cred->twitchid), true);

        $live = "";
        $offline = "";
        $ct = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='check_time'", OBJECT);
        $ct = $ct[0]->cfg_value;
        for($i = 0; $i < $tapi["_total"]; $i ++) {
            $live .= $tapi["streams"][$i]["channel"]["name"] . ",";
            $counted = false;
            foreach(explode(",", $watching) as $watch) {
                if($watch == $tapi["streams"][$i]["channel"]["name"])
                    $counted = true;
            }

            if(!$counted) {
                $wpdb->query("INSERT INTO wp_sqyy0tvj8s_tcred_streams (twitch, start_time, next_check) VALUES
             ('" . $tapi["streams"][$i]["channel"]["name"] . "', '" . time() . "', '" . (time() + $ct) . "')");
            }
        }

        foreach(explode(",", $watching) as $watch) {
            $result = false;
            foreach(explode(",", $live) as $online) {
                if($online == $watch)
                    $result = true;
            }

            if($result == false) {
                // No longer online tho!
                $offline .= $watch . ",";
                $wpdb->query("DELETE FROM wp_sqyy0tvj8s_tcred_streams WHERE twitch='$watch'");
            }
            else {
                // Just update check time
                $wpdb->query("UPDATE wp_sqyy0tvj8s_tcred_streams SET next_check='" . (time() + $ct) . "' WHERE twitch='$watch'");
            }
        }

        echo $live . ":" . $offline;
        break;
}
