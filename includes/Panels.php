<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 07.10.2016
 * Time: 19:39
 */
global $wpdb;

if (isset($_GET["view"])) {
    ?>
    <tr class="header">
        <td class="manage-column" align="center">
            Streamer
        </td>
        <td class="manage-column" align="center">
            Watcher
        </td>
        <td class="manage-column" align="center">
            Updated
        </td>
        <td class="manage-column" align="center">
            Coins earned this session
        </td>
        <td class="manage-column" align="center">
            Online Time
        </td>
    </tr>
    <?php
    $res = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_online", OBJECT);
    $i = 0;
    if (count($res) == 0) {
        echo "<tr>";
        echo "<td align=\"center\"  class=\"manage-column\"></td>";
        echo "<td align=\"center\"  class=\"manage-column\"></td>";
        echo "<td align=\"center\"  class=\"manage-column\">NO VIEWERS ONLINE</td>";
        echo "<td align=\"center\"  class=\"manage-column\"></td>";
        echo "<td align=\"center\"  class=\"manage-column\"></td>";
        echo "</tr>";
    }

    foreach ($res as $r) {
        $wpu = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_users WHERE user_nicename='" . $r->watcher . "'");
        $b = "";
        if (count($wpu) == 0) $b = "<img src='https://cdn2.iconfinder.com/data/icons/function_icon_set/cancel_16.png'>";
        else $b = "";
        if ($r->streamer == $r->watcher) $b .= "<img src='http://www.pmsolutions.com/images/alert.png'>";


        if (tcred_debug())
            $b = "";

        $dateD = new DateTime();
        $dateD->setTimestamp($r->updated);
        $datediff = date_diff(date_create(date("Y-m-d G:i:s", $r->start_time)), date_create(date("Y-m-d G:i:s")));

        $months = $datediff->m + ($datediff->y > 1 ? ($datediff->y * 12) : 0);
        $days = $datediff->d;

        $format = "";

        if ($days > 0) {
            if ($datediff->y > 0) $format .= $datediff->y . ($datediff->y == 1 ? " Year, " : " Years, ");
            if ($months > 0) $format .= $months . ($months == 1 ? " Month, " : " Months, ");
            $format .= $days . ($days == 1 ? " Day" : " Days");
        } else {
            if ($datediff->h > 0) $format .= $datediff->h . ($datediff->h == 1 ? " Hour, " : " Hours, ");
            if ($datediff->i > 0) $format .= $datediff->i . ($datediff->i == 1 ? " Minute, " : " Minutes, ");
            if ($datediff->s > 0) $format .= $datediff->s . ($datediff->s == 1 ? " Second" : " Seconds");
        }

        echo "<tr>";
        echo "<td align=\"center\" class=\"manage-column\">" . $r->streamer . "</td>";
        echo "<td align=\"center\" class=\"manage-column\">$b " . $r->watcher . "</td>";
        echo "<td align=\"center\" class=\"manage-column\">" . $dateD->format('Y-m-d H:i:s') . "</td>";
        echo "<td align=\"center\" class=\"manage-column\">" . ($b == "" ? $r->coins_earned : "---") . "</td>";
        echo "<td align=\"center\" class=\"manage-column\">$format</td>";
        echo "</tr>";

        $i++;
    }
    die();
}

add_action("admin_menu", "tcred_create_menu");
add_action('admin_init', 'tcred_settings');

function tcred_create_menu()
{
    add_menu_page("twitchCRED", "twitchCRED", "administrator", "aresak.twitchcred", "twitchcred_p", plugins_url() . "/twitchCRED/media/twitch.png");

    add_submenu_page("aresak.twitchcred", "Viewers", "Viewers", "administrator", "aresak.twitchcred.streams", "twitchcred_f_streams");
    add_submenu_page("aresak.twitchcred", "Streams", "Streams", "administrator", "aresak.twitchcred.online", "twitchcred_f_online");
    add_submenu_page("aresak.twitchcred", "[M] Whispers", "[M] Whispers", "administrator", "aresak.twitchcred.whispers", "twitchcred_f_whispers");
    add_submenu_page("aresak.twitchcred", "Debugger", "Debugger", "administrator", "aresak.twitchcred.debugger", "twitchcred_f_debugger");
}


function tcred_settings()
{
    register_setting("twitchcred-stgg", "Overview");
    register_setting("twitchcred-stgg", "Viewers");
}


function twitchcred_f_debugger()
{
    global $wpdb;
    if (isset($_POST)) {
        $action = $_POST["action"];

        if ($action == "reset_streams") {
            $wpdb->query("DELETE FROM wp_sqyy0tvj8s_tcred_streams WHERE 1=1");
        } else if ($action == "reset_online") {
            $wpdb->query("DELETE FROM wp_sqyy0tvj8s_tcred_online WHERE 1=1");
        }
    }
    ?>
    <h1>twitchCRED
        <small>BY ARESAK</small>
    </h1>
    <b>DEBUGGER</b> <i>
    <small><?php echo tcred_version(); ?></small>
</i><br>
    <div class="atc-menu">
        <a class="atc-menu-item" href="?page=aresak.twitchcred.streams">
            View Viewers
        </a>
        <a class="atc-menu-item" href="?page=aresak.twitchcred.online">
            View Streams
        </a>
    </div>

    <div id="atc-block">
        <form method="post">
            <button name="action" value="reset_streams">
                Reset Viewers
            </button>

            <button name="action" value="reset_online">
                Reset Streams
            </button>
        </form>
    </div>
    <?php
}

function twitchcred_p()
{
    if (isset($_POST)) {
        if (isset($_POST["atcf-timer"])) {
            // SET NEW VALUES
            $t = $_POST["atcf-timer"];
            $p = $_POST["atcf-points"];
            $ct = $_POST["atcf-ct"];

            global $wpdb;
            $wpdb->update($wpdb->prefix . "tcred_config", array("cfg_value" => $t), array("cfg_name" => "increase_time"));
            $wpdb->update($wpdb->prefix . "tcred_config", array("cfg_value" => $p), array("cfg_name" => "increase_points"));
            $wpdb->update($wpdb->prefix . "tcred_config", array("cfg_value" => $ct), array("cfg_name" => "check_time"));
        }
    }


    ?>
    <h1>twitchCRED
        <small>BY ARESAK</small>
    </h1>
    <b>CONFIG</b> <i>
    <small><?php echo tcred_version(); ?></small>
</i><br>
    <div class="atc-menu">
        <a class="atc-menu-item" href="?page=aresak.twitchcred.streams">
            View Viewers
        </a>
        <a class="atc-menu-item" href="?page=aresak.twitchcred.online">
            View Streams
        </a>
    </div>

    <div id="atc-block">
        <form method="post">
            <table class="widefat">
                <input type="hidden" name="page" value="aresak.twitchcred">
                <tr>
                    <td class="manage-column" align="right">
                        Timer for bot:
                    </td>
                    <td class="manage-column" align="left">
                        <input type="number" name="atcf-timer" value="<?php echo tcred_c_timer(); ?>">
                    </td>
                </tr>
                <tr>
                    <td class="manage-column" align="right">
                        Points each cycle:
                    </td>
                    <td align="left">
                        <input type="number" name="atcf-points" value="<?php echo tcred_c_points(); ?>">
                    </td>
                </tr>
                <tr>
                    <td class="manage-column" align="right">
                        Bot check time:
                    </td>
                    <td align="left">
                        <input type="number" name="atcf-ct" value="<?php echo tcred_c_check(); ?>">
                    </td>
                </tr>
                <tr class="alternate">
                    <td>
                        <button>UPDATE</button>
                    </td>
                    <td></td>
                </tr>
            </table>
        </form>
    </div>
    <?php
}

function twitchcred_f_online()
{
    global $wpdb;
    ?>
    <h1>twitchCRED
        <small>BY ARESAK</small>
    </h1>
    <b>Streams - Bot watches <i>
            <small><?php echo tcred_version(); ?></small>
        </i></b>
    <div class="atc-menu">
        <a class="atc-menu-item" href="?page=aresak.twitchcred.streams">
            View Viewers
        </a>
        <a class="atc-menu-item" href="?page=aresak.twitchcred">
            View Overview
        </a>
    </div>

    <div id="atc-block">
        <table class="widefat">
            <tr class="header">
                <td class="manage-column" align="center">
                    Twitch
                </td>
                <td class="manage-column" align="center">
                    Online since
                </td>
                <td class="manage-column" align="center">
                    Next status check
                </td>
            </tr>
            <?php
            $res = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_streams");
            foreach ($res as $r) {
                $t = $r->next_check - time();
                $minutes = round($t / 60);
                $seconds = $t % 60;

                echo "<tr>";
                echo "<td class=\"manage-column\" align=\"center\">" . $r->twitch . "</td>";
                echo "<td class=\"manage-column\" align=\"center\">" . formateTime(time(), $r->start_time) . "</td>";
                echo "<td class=\"manage-column\" align=\"center\">" . $minutes . "m " . $seconds . "s</td>";
                echo "</tr>";
            }
            ?>
        </table>
    </div>
    <?php
}

function twitchcred_f_streams()
{
    global $wpdb;
    if (isset($_POST)) {
        // actions

    }

    ?>
    <h1>twitchCRED
        <small>BY ARESAK</small>
    </h1>
    <b>Connected users</b> <i>
    <small><?php echo tcred_version(); ?></small>
</i><br>
    <div class="atc-menu">
        <a class="atc-menu-item" href="?page=aresak.twitchcred">
            View Overview
        </a>
        <a class="atc-menu-item" href="?page=aresak.twitchcred.online">
            View Viewers
        </a>
    </div>

    <div id="atc-block">
        <div id="atc-online-list">
            <table id="list-refreshed" class="widefat">

            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>
        function refresher() {
            $.get(location.pathname, {page: "aresak.twitchcred.streams", view: true}, function (data) {
                $("#list-refreshed").html(data);
                setTimeout(refresher, 5000);
            }, "html");
        }
        setTimeout(refresher, 500);
    </script>
    <?php
}

function twitchcred_f_whispers()
{
    global $wpdb;
    if (isset($_POST)) {
        // actions

        if(isset($_POST["action"])) {
            $a = $_POST["action"];
            if($a == "wmu") {
                if(isset($_POST["delay"]) && isset($_POST["message"]) && isset($_POST["hash"])) {
                    $delay = $_POST["delay"];
                    $message = $_POST["message"];
                    $hash = $_POST["hash"];

                    $wpdb->query("UPDATE wp_sqyy0tvj8s_tcred_whispers SET delay='$delay', message='$message' WHERE hash='$hash'");
                }
            }
            else if($a == "wmd") {
                if(isset($_POST["hash"])) {
                    $hash = $_POST["hash"];

                    $wpdb->query("DELETE FROM wp_sqyy0tvj8s_tcred_whispers WHERE hash='$hash'");
                }
            }
            else if($a == "wma") {
                if(isset($_POST["delay"]) && isset($_POST["message"])) {
                    $delay = $_POST["delay"];
                    $message = $_POST["message"];
                    $hash = time();

                    $wpdb->query("INSERT INTO wp_sqyy0tvj8s_tcred_whispers (hash, message, delay) VALUES ('$hash', '$message', '$delay')");
                }
            }
            else if($a == "wcfg") {
                if(isset($_POST["delay"]) && isset($_POST["enabled"])) {
                    $delay = $_POST["delay"];
                    $enabled = $_POST["enabled"];
                    if($enabled == "true")
                        $b = 1;
                    else
                        $b = 0;

                    $wpdb->query("UPDATE wp_sqyy0tvj8s_tcred_whispers SET delay='$delay' WHERE message='_globalDelay'");
                    $wpdb->query("UPDATE wp_sqyy0tvj8s_tcred_whispers SET delay='$b' WHERE message='_globalDelayEnabled'");
                }
            }
        }
    }


    // loading messages
    $messages = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_whispers", OBJECT);

    $wcfg_gdenabled = "false";
    $wcfg_gdelay = 0;

    $w = array(false, false);
    foreach ($messages as $message) {
        if ($message->hash == 0) {
            if ($message->message == "_globalDelay") {
                $w[0] = true;
                $wcfg_gdelay = $message->delay;
                if ($w[1])
                    break;
            } else if ($message->message == "_globalDelayEnabled") {
                $w[1] = true;
                $wcfg_gdenabled = $message->delay;
                if ($w[0])
                    break;
            }
        }
    }
    ?>
    <h1>twitchCRED
        <small>BY ARESAK</small>
    </h1>
    <b>[MODULE] Whispers</b> <i>
    <small><?php echo tcred_version(); ?></small>
</i><br>
    <div class="atc-menu">
        <a class="atc-menu-item" href="?page=aresak.twitchcred">
            View Overview
        </a>
        <a class="atc-menu-item" href="?page=aresak.twitchcred.online">
            View Viewers
        </a>
    </div>

    <div id="atc-block">
        <div id="atc-cfg-list">
            <table id="atc-cfg-table" class="widefat">
                <tr>
                    <td align="right">Global Delay Enabled:</td>
                    <td align="left"><input type="checkbox" id="atc-wcfg-gdenabled"
                                            value="<?php echo $wcfg_gdenabled; ?>"></td>

                    <td></td><td></td>
                    <td align="right">Delay</td>
                    <td align="left"><input type="number" min="1" id="atc-wam-delay"></td>
                </tr>

                <tr>
                    <td align="right">Global Delay: <i>(in seconds)</i></td>
                    <td align="left"><input id="atc-wcfg-gdelay" type="number" min="1"
                                            value="<?php echo $wcfg_gdelay; ?>"></td>
                    <td></td><td></td>
                    <td align="right">Message</td>
                    <td align="left"><input type="text" id="atc-wam-message"></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <button onClick="atc_wcfg_update();">Update Global Delay Config</button>
                    </td>
                    <td></td><td></td>
                    <td></td>
                    <td align="left"><button onClick="atc_wam_add();">ADD MESSAGE</button></td>
                </tr>
            </table>
        </div>
        <div id="atc-whispers-list">
            <table id="atc-list-table" class="widefat">
                <tr>
                    <td align="center">
                        <b>
                            ID
                        </b>
                    </td>
                    <td align="center">
                        <b>
                            HASH
                        </b>
                    </td>
                    <td align="center">
                        <b>
                            DELAY
                        </b>
                    </td>
                    <td align="center">
                        <b>
                            MESSAGE
                        </b>
                    </td>
                    <td align="center">
                    </td>
                </tr>
                <?php

                foreach ($messages as $message) {
                    if ($message->hash != 0) {
                        echo "<tr id='wm-" . $message->hash . "-row'>";
                        echo "<td align='center'>" . $message->ID . "</td>";
                        echo "<td align='center' id='wm-" . $message->hash . "-hash'>" . md5($message->hash) . "</td>";
                        echo "<td align='center' id='wm-" . $message->hash . "-delay'>" . $message->delay . "</td>";
                        echo "<td align='center' id='wm-" . $message->hash . "-msg'>" . $message->message . "</td>";
                        echo "<td align='center'><button id='wm-" . $message->hash . "-abtn' onclick='atc_wm_edit(\"" . $message->hash . "\")'>EDIT</button>
                                <button onClick='atc_wm_delete(\"" . $message->hash . "\")'>DELETE</button></td>";
                        echo "</tr>";
                    }
                }

                ?>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>
        function atc_wcfg_update() {
            var enabled = $("#atc-wcfg-gdenabled");
            var delay = $("#atc-wcfg-gdelay");

            $.post("", {action: "wcfg", enabled: enabled.is(":checked"), delay: delay.val()}, function() {
                location.reload();
            }, "html");
        }

        function atc_wam_add() {
            var message = $("#atc-wam-message");
            var delay = $("#atc-wam-delay");

            $.post("", {action: "wma", message: message.val(), delay: delay.val()}, function() {
                message.val("");
                delay.val("");
                location.reload();
            }, "html");
        }

        function atc_wm_edit(hash) {
            var delayTD = $("#wm-" + hash + "-delay");
            var messageTD = $("#wm-" + hash + "-msg");
            var buttonTD = $("#wm-" + hash + "-abtn");

            delayTD.html("<input type='number' min='1' id='wm-" + hash + "-delay-value' value='" + delayTD.text() + "'>");
            messageTD.html("<textarea id='wm-" + hash + "-msg-value'>" + messageTD.text() + "</textarea>");

            $("#wm-" + hash + "-row").css("background-color", "#efefef");

            buttonTD.text("UPDATE");
            buttonTD.attr("onClick", "atc_wm_update(\"" + hash + "\");");
        }

        function atc_wm_update(hash) {
            var delayTD = $("#wm-" + hash + "-delay");
            var messageTD = $("#wm-" + hash + "-msg");
            var buttonTD = $("#wm-" + hash + "-abtn");

            var delayVAL = $("#wm-" + hash + "-delay-value");
            var messageVAL = $("#wm-" + hash + "-msg-value");

            $("#wm-" + hash + "-row").css("background-color", "#ffd972");

            $.post("", {action: "wmu", hash: hash, delay: delayVAL.val(), message: messageVAL.val()}, function() {
                delayTD.html(delayVAL.val());
                messageTD.html(messageVAL.val());
                $("#wm-" + hash + "-row").css("background-color", "white");
            }, "html");


            buttonTD.text("EDIT");
            buttonTD.attr("onClick", "atc_wm_edit(\"" + hash + "\");");
        }

        function atc_wm_delete(hash) {
            $("#wm-" + hash + "-row").css("background-color", "red");

            $.post("", {action: "wmd", hash: hash}, function() {
                $("#wm-" + hash + "-row").hide();
            }, "html");
        }
    </script>
    <?php
}