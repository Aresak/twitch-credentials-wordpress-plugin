<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 07.10.2016
 * Time: 19:36
 * Plugin Name: twitchCRED
 * Description: Aresak's WP plugin for DGI. Twitch + myCRED + WP integration
 */
global $wpdb;
function tcred_twitch_clientid() {
    return "";
}

add_shortcode("tcred-c-timer", "tcred_c_timer");
add_shortcode("tcred-c-points", "tcred_c_points");


function tcred_version() {
    return "1.0.13";
}

function tcred_debug() {
    return false;
}

function formateTime($unix1, $unix2) {
    $datediff = date_diff(date_create(date("Y-m-d G:i:s", $unix2)), date_create(date("Y-m-d G:i:s", $unix1)));

    $months = $datediff->m + ($datediff->y > 1 ? ($datediff->y * 12) : 0);
    $days   = $datediff->d;

    $format = "";

    if($days > 0) {
        if ($datediff->y > 0) $format .= $datediff->y . ($datediff->y == 1 ? " Year, " : " Years, ");
        if ($months > 0) $format .= $months . ($months == 1 ? " Month, " : " Months, ");
        $format .= $days . ($days == 1 ? " Day" : " Days");
    }
    else {
        if($datediff->h > 0) $format .= $datediff->h . ($datediff->h == 1 ? " Hour, " : " Hours, ");
        if($datediff->i > 0) $format .= $datediff->i . ($datediff->i == 1 ? " Minute, " : " Minutes, ");
        if($datediff->s > 0) $format .= $datediff->s . ($datediff->s == 1 ? " Second" : " Seconds");
    }

    return $format;
}


function tcred_check_tables($wpdb) {
    $tables = array(
        "CREATE TABLE IF NOT EXISTS `wp_sqyy0tvj8s_tcred_config` (
          `ID` int(11) NOT NULL AUTO_INCREMENT,
          `cfg_name` text NOT NULL,
          `cfg_value` text NOT NULL,
          PRIMARY KEY (`ID`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;",
        "CREATE TABLE IF NOT EXISTS `wp_sqyy0tvj8s_tcred_streams` (
          `ID` int(11) NOT NULL AUTO_INCREMENT,
          `next_check` int(11) NOT NULL,
          `start_time` int(11) NOT NULL,
          `twitch` text NOT NULL,
          PRIMARY KEY (`ID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;",
        "CREATE TABLE IF NOT EXISTS `wp_sqyy0tvj8s_tcred_online` (
          `ID` int(11) NOT NULL AUTO_INCREMENT,
          `streamer` text NOT NULL,
          `watcher` text NOT NULL,
          `updated` int(11) NOT NULL,
          `start_time` int(11) NOT NULL,
          `coins_earned` int(11) NOT NULL,
          PRIMARY KEY (`ID`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;",
            "INSERT INTO `wp_sqyy0tvj8s_tcred_config` (`ID`, `cfg_name`, `cfg_value`) VALUES
            (1, 'increase_time', '180'),
            (2, 'increase_points', '5'),
            (3, 'check_time', '300');",
        "CREATE TABLE IF NOT EXISTS `wp_sqyy0tvj8s_tcred_whispers` (
              `ID` int(11) NOT NULL AUTO_INCREMENT,
              `hash` int(11) NOT NULL,
              `message` text NOT NULL,
              `delay` int(11) NOT NULL,
              PRIMARY KEY (`ID`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;",
            "INSERT INTO `wp_sqyy0tvj8s_tcred_whispers` (`ID`, `hash`, `message`, `delay`) VALUES
            (1, 0, '_globalDelay', 20),
            (2, 0, '_globalDelayEnabled', 0);"
    );

    foreach($tables as $t) {
        $wpdb->query($t);
    }
}

function tcred_c_timer() {
    global $wpdb;
    $c = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='increase_time'", OBJECT);
    return $c[0]->cfg_value;
}

function tcred_c_points() {
    global $wpdb;
    $c = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='increase_points'", OBJECT);
    return $c[0]->cfg_value;
}

function tcred_c_check() {
    global $wpdb;
    $c = $wpdb->get_results("SELECT * FROM wp_sqyy0tvj8s_tcred_config WHERE cfg_name='check_time'", OBJECT);
    return $c[0]->cfg_value;
}


tcred_check_tables($wpdb);
require_once(plugin_dir_path( __FILE__ ) . "/includes/Panels.php");
